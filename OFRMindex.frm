VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form OFRMindex 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "JOEY - The BMI Expert!"
   ClientHeight    =   7725
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10590
   Icon            =   "OFRMindex.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7725
   ScaleWidth      =   10590
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame21 
      BackColor       =   &H8000000E&
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8775
      Begin VB.ComboBox cmbMainTopic 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "OFRMindex.frx":17D2A
         Left            =   120
         List            =   "OFRMindex.frx":17D3A
         Style           =   2  'Dropdown List
         TabIndex        =   45
         Top             =   480
         Width           =   3615
      End
      Begin VB.ComboBox cmbSubTopic 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "OFRMindex.frx":17D84
         Left            =   3840
         List            =   "OFRMindex.frx":17D86
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   480
         Width           =   4815
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Please Choose a topic:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   2655
      End
      Begin VB.Label Label11 
         BackColor       =   &H8000000D&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   375
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   8775
      End
      Begin VB.Label Label26 
         BackStyle       =   0  'Transparent
         Caption         =   "Choose a topic"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000E&
         Height          =   255
         Left            =   135
         TabIndex        =   1
         Top             =   120
         Width           =   1335
      End
   End
   Begin VB.Frame Sorry 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   42
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin VB.CommandButton cmdselectq 
         Caption         =   "I would like to select another question..."
         Height          =   495
         Left            =   2955
         TabIndex        =   43
         Top             =   2400
         Width           =   4695
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Sorry, but JOEY cannot find an answer to your question."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   2535
         TabIndex        =   44
         Top             =   1560
         Width           =   5535
      End
   End
   Begin VB.Frame Thankyou 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   38
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin VB.CommandButton cmdcategory 
         Caption         =   "I would like to select another question..."
         Height          =   495
         Left            =   2955
         TabIndex        =   39
         Top             =   2400
         Width           =   4695
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "Thank you for using this expert system!"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2535
         TabIndex        =   40
         Top             =   1800
         Width           =   5535
      End
   End
   Begin VB.Frame Answer 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   31
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin VB.CommandButton cmdno 
         Caption         =   "No"
         Height          =   255
         Left            =   8640
         TabIndex        =   37
         Top             =   6000
         Width           =   1455
      End
      Begin VB.CommandButton cmdyes 
         Caption         =   "Yes"
         Height          =   255
         Left            =   7080
         TabIndex        =   36
         Top             =   6000
         Width           =   1455
      End
      Begin VB.CommandButton cmdanswer2 
         Caption         =   "Show me another answer"
         Height          =   495
         Left            =   3480
         TabIndex        =   35
         Top             =   5760
         Width           =   3255
      End
      Begin VB.CommandButton cmdreturn 
         Caption         =   "Return to my questions"
         Height          =   495
         Left            =   120
         TabIndex        =   33
         Top             =   5760
         Width           =   3255
      End
      Begin RichTextLib.RichTextBox Answertb 
         Height          =   4335
         Left            =   120
         TabIndex        =   32
         Top             =   1320
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   7646
         _Version        =   393217
         BorderStyle     =   0
         Enabled         =   -1  'True
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         TextRTF         =   $"OFRMindex.frx":17D88
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label lblq 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   735
         Left            =   120
         TabIndex        =   47
         Top             =   240
         Width           =   10095
      End
      Begin VB.Label lblpa 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   1080
         Width           =   10095
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Are you satisfied with the answer?"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   7080
         TabIndex        =   41
         Top             =   5760
         Width           =   3015
      End
   End
   Begin VB.Frame Definition 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   6
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin RichTextLib.RichTextBox rtb1 
         Height          =   6015
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   10095
         _ExtentX        =   17806
         _ExtentY        =   10610
         _Version        =   393217
         BorderStyle     =   0
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         Appearance      =   0
         TextRTF         =   $"OFRMindex.frx":17E0A
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Category 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   12
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":17E8C
         MousePointer    =   99  'Custom
         TabIndex        =   30
         Top             =   4560
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   11
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":18196
         MousePointer    =   99  'Custom
         TabIndex        =   29
         Top             =   4200
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   10
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":184A0
         MousePointer    =   99  'Custom
         TabIndex        =   28
         Top             =   3840
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   9
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":187AA
         MousePointer    =   99  'Custom
         TabIndex        =   27
         Top             =   3480
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   8
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":18AB4
         MousePointer    =   99  'Custom
         TabIndex        =   26
         Top             =   3120
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   7
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":18DBE
         MousePointer    =   99  'Custom
         TabIndex        =   25
         Top             =   2760
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   6
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":190C8
         MousePointer    =   99  'Custom
         TabIndex        =   24
         Top             =   2400
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   5
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":193D2
         MousePointer    =   99  'Custom
         TabIndex        =   23
         Top             =   2040
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   4
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":196DC
         MousePointer    =   99  'Custom
         TabIndex        =   22
         Top             =   1680
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   3
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":199E6
         MousePointer    =   99  'Custom
         TabIndex        =   21
         Top             =   1320
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   2
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":19CF0
         MousePointer    =   99  'Custom
         TabIndex        =   20
         Top             =   960
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Label Label1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H8000000D&
         Height          =   255
         Index           =   1
         Left            =   240
         MouseIcon       =   "OFRMindex.frx":19FFA
         MousePointer    =   99  'Custom
         TabIndex        =   19
         Top             =   600
         Visible         =   0   'False
         Width           =   9615
      End
      Begin VB.Image Image3 
         Height          =   3960
         Left            =   4800
         Picture         =   "OFRMindex.frx":1A304
         Stretch         =   -1  'True
         Top             =   1200
         Width           =   4620
      End
   End
   Begin VB.Frame Questions 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   8
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin VB.CommandButton cmdback 
         Caption         =   "Return to categories"
         Height          =   375
         Left            =   240
         TabIndex        =   34
         Top             =   5880
         Width           =   3615
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Show me the answer"
         Height          =   375
         Left            =   3960
         TabIndex        =   18
         Top             =   5880
         Width           =   3615
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   9
         Left            =   240
         TabIndex        =   17
         Top             =   5280
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   8
         Left            =   240
         TabIndex        =   16
         Top             =   4680
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   7
         Left            =   240
         TabIndex        =   15
         Top             =   4080
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   6
         Left            =   240
         TabIndex        =   14
         Top             =   3480
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   5
         Left            =   240
         TabIndex        =   13
         Top             =   2880
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   4
         Left            =   240
         TabIndex        =   12
         Top             =   2280
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   240
         TabIndex        =   11
         Top             =   1680
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Visible         =   0   'False
         Width           =   9735
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   240
         TabIndex        =   9
         Top             =   480
         Visible         =   0   'False
         Width           =   9735
      End
   End
   Begin VB.Frame Home 
      BackColor       =   &H00FFFFFF&
      Height          =   6375
      Left            =   120
      TabIndex        =   48
      Top             =   1200
      Visible         =   0   'False
      Width           =   10335
      Begin VB.Image Image1 
         Height          =   3960
         Left            =   2985
         Picture         =   "OFRMindex.frx":1B4D5
         Stretch         =   -1  'True
         Top             =   960
         Width           =   4620
      End
   End
End
Attribute VB_Name = "OFRMindex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim rc As ADODB.Recordset

Dim AnswerIndex As Integer
Dim x As Integer
Dim tablename As String
Dim PossibleAnswer As Integer
Dim current_answer As String

Private Sub cmbMainTopic_Click()
Select Case cmbMainTopic.ListIndex
    Case 0
        cmbSubTopic.Clear
        cmbSubTopic.AddItem "- Joey Index -"
        cmbSubTopic.ListIndex = 0
        
    Case 1 'OBESITY
        cmbSubTopic.Clear
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM Sub_topic_list WHERE Category='Obesity'", conJoey, adOpenForwardOnly, adLockReadOnly
            List_SubTopics (rc.RecordCount)
            cmbSubTopic.ListIndex = 0
            cmdback.Visible = True
            
    Case 2 'MALNUTRITION
        cmbSubTopic.Clear
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM Sub_topic_list WHERE Category='Malnutrition'", conJoey, adOpenForwardOnly, adLockReadOnly
            List_SubTopics (rc.RecordCount)
            cmbSubTopic.ListIndex = 0
            cmdback.Visible = True
            
    Case 3 'GUIDELINES AND RECOMMENDATION
        cmbSubTopic.Clear
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM Sub_topic_list WHERE Category='Guidelines'", conJoey, adOpenForwardOnly, adLockReadOnly
            List_SubTopics (rc.RecordCount)
            cmbSubTopic.ListIndex = 0
            cmdback.Visible = False
End Select
End Sub

Sub List_SubTopics(ByVal TotalRecords As Integer)
For x = 1 To TotalRecords
    cmbSubTopic.AddItem rc("Topic")
    rc.MoveNext
Next x
End Sub

Private Sub cmbMainTopic_KeyPress(KeyAscii As Integer)
KeyAscii = 0
cmbMainTopic.BackColor = &H80000005
End Sub

Private Sub cmbSubTopic_Click()
Select Case cmbMainTopic.ListIndex
Case 0
    If cmbSubTopic.Text = "- Joey Index -" Then
        show_home
    End If
    
Case 1 'OBESITY
    Select Case cmbSubTopic.Text
        Case "Definition"
            Definition.Visible = True
            Questions.Visible = False
            Category.Visible = False
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM kb_definitions WHERE Keyword='Obesity'", conJoey, adOpenForwardOnly, adLockReadOnly
                rtb1.Text = rc("Definition")
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM kb_causes WHERE Keyword='Obesity'", conJoey, adOpenForwardOnly, adLockReadOnly
                rtb1.Text = rtb1.Text & vbNewLine & vbNewLine & rc("Causes")
                
            
        Case "Causes"
            clear_prev_q
            show_category
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM topic_list WHERE Category='Obesity-causes'", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Label1(x).Caption = rc("Topic")
                Label1(x).Visible = True
                rc.MoveNext
            Next x
    
        Case "Body Mass Index"
            clear_prev_q
            show_category
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM topic_list WHERE Category='Obesity-bmi'", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Label1(x).Caption = rc("Topic")
                Label1(x).Visible = True
                rc.MoveNext
            Next x
        
        Case "Treatment"
            clear_prev_q
            show_category
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM topic_list WHERE Category='Obesity-treatment'", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Label1(x).Caption = rc("Topic")
                Label1(x).Visible = True
                rc.MoveNext
            Next x
            
        Case "Evaluating Risk Factor"
            clear_prev_q
            show_category
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM topic_list WHERE Category='Obesity-riskfactor'", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Label1(x).Caption = rc("Topic")
                Label1(x).Visible = True
                rc.MoveNext
            Next x
    End Select
    cmdback.Visible = True
    
Case 2 'Malnutrition
    Select Case cmbSubTopic.Text
        Case "Definition"
            Definition.Visible = True
            Questions.Visible = False
            Category.Visible = False
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM kb_definitions WHERE Keyword='Malnutrition'", conJoey, adOpenForwardOnly, adLockReadOnly
                rtb1.Text = rc("Definition")
                
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM kb_causes WHERE Keyword='Malnutrition'", conJoey, adOpenForwardOnly, adLockReadOnly
                rtb1.Text = rtb1.Text & vbNewLine & vbNewLine & rc("Causes")
            
        Case "Dietary Management"
            clear_prev_q
            show_category
            
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM topic_list WHERE Category='Malnutrition-dm'", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Label1(x).Caption = rc("Topic")
                Label1(x).Visible = True
                rc.MoveNext
            Next x
        End Select
        cmdback.Visible = True

Case 3 'Guidelines
    Select Case cmbSubTopic.Text 'THIS WILL DISPLAY QUESTIONS IMMIDIATELY
        Case "Healthy Lifestyle"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_guidelines_hl", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_guidelines_hl"
            show_questions
     
        Case "Nutrition Guidelines and Proper Diet"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_guidelines_ng", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_guidelines_ng"
            show_questions
        End Select
        cmdback.Visible = False
End Select
End Sub

Private Sub cmdanswer2_Click()
seek_questions
If current_answer = "original" Then
    Answertb.Text = rc("answer1")
    current_answer = "secondary"
    
ElseIf current_answer = "secondary" And PossibleAnswer = 3 Then
    Answertb.Text = rc("answer")
    current_answer = "terciary"

ElseIf current_answer = "secondary" And PossibleAnswer = 2 Then
    Answertb.Text = rc("answer")
    current_answer = "original"

ElseIf current_answer = "terciary" Then
    Answertb.Text = rc("answer2")
    current_answer = "original"
End If
End Sub

Private Sub cmdback_Click()
show_category
End Sub

Private Sub cmdcategory_Click()
show_category
End Sub

Private Sub cmdno_Click()
show_sorry
End Sub

Private Sub cmdreturn_Click()
Answer.Visible = False
Questions.Visible = True
reset_selections
End Sub

Private Sub cmdselectq_Click()
show_questions
reset_selections
End Sub

Private Sub cmdyes_Click()
show_thankyou
End Sub

Sub reset_selections()
For x = 1 To Option1.Count
    Option1(x).Value = False
Next x
End Sub

Sub clear_prev_q()
For x = 1 To Label1.Count 'Clears visible label first
    Label1(x).Visible = False
Next x
End Sub

Sub clear_prev_c()
For x = 1 To Option1.Count
    Option1(x).Visible = False
Next x
End Sub

Sub show_home()
Home.Visible = True
Definition.Visible = False
Questions.Visible = False
Category.Visible = False
Answer.Visible = False
Thankyou.Visible = False
Sorry.Visible = False
End Sub

Sub show_category()
Home.Visible = False
Definition.Visible = False
Questions.Visible = False
Category.Visible = True
Answer.Visible = False
Thankyou.Visible = False
Sorry.Visible = False
End Sub

Sub show_thankyou()
Home.Visible = False
Definition.Visible = False
Category.Visible = False
Questions.Visible = False
Answer.Visible = False
Thankyou.Visible = True
Sorry.Visible = False
End Sub

Sub show_sorry()
Home.Visible = False
Definition.Visible = False
Category.Visible = False
Questions.Visible = False
Answer.Visible = False
Thankyou.Visible = False
Sorry.Visible = True
End Sub

Sub show_questions()
Home.Visible = False
Definition.Visible = False
Category.Visible = False
Questions.Visible = True
Answer.Visible = False
Thankyou.Visible = False
Sorry.Visible = False
cmdback.Visible = True
End Sub

Sub show_answer()
Home.Visible = False
Definition.Visible = False
Category.Visible = False
Questions.Visible = False
Answer.Visible = True
Thankyou.Visible = False
Sorry.Visible = False
End Sub

Private Sub Command1_Click()
On Error GoTo error_handler
seek_questions

Answertb.Text = rc("answer")
PossibleAnswer = rc("possible_answer")

If PossibleAnswer = 1 Then
    lblq.Caption = "Your question is: " & Option1(AnswerIndex).Caption
    lblpa.Caption = "JOEY: I can only give you 1 possible answer."
    cmdanswer2.Enabled = False
ElseIf PossibleAnswer = 2 Then
    lblq.Caption = "Your question is: " & Option1(AnswerIndex).Caption
    lblpa.Caption = "JOEY: I can show you 2 possible answer."
    cmdanswer2.Enabled = True
ElseIf PossibleAnswer = 3 Then
    lblq.Caption = "Your question is: " & Option1(AnswerIndex).Caption
    lblpa.Caption = "JOEY: I can show you 3 possible answer."
    cmdanswer2.Enabled = True
End If

current_answer = "original"
show_answer
Exit Sub
error_handler:
Select Case Err.Number
    Case 3021
        MsgBox "Please select the question you want.", vbExclamation, "No question selected"
        Exit Sub
End Select
End Sub

Sub seek_questions()
Select Case AnswerIndex
    Case 1
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=1", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 2
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=2", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 3
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=3", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 4
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=4", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 5
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=5", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 6
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=6", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 7
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=7", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 8
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=8", conJoey, adOpenForwardOnly, adLockReadOnly
    Case 9
        Set rc = New ADODB.Recordset
            rc.Open "SELECT * FROM " & tablename & " WHERE qindex=9", conJoey, adOpenForwardOnly, adLockReadOnly
End Select
End Sub

Private Sub Form_Load()
connect_Joey
cmbMainTopic.ListIndex = 0
Home.Visible = True
End Sub

Private Sub Label1_Click(Index As Integer)
'SUB CATEGORIES
Select Case Label1(Index).Caption
'----------------------BEGIN TOPIC: OBESITY
    '----BEGIN SUBTOPIC - CAUSES
        Case "Family History"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_familyhistory", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_obesity_familyhistory"
            show_questions
        
        Case "Environment"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_environment", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_obesity_environment"
            show_questions
        
        Case "Nutrition"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_nutrition", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            
            tablename = "q_obesity_nutrition"
            show_questions
        
        Case "Illnesses"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_illness", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            
            tablename = "q_obesity_illness"
            show_questions
        
        Case "Sociocultural Factors"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_socioculturalfactors", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            
            tablename = "q_obesity_socioculturalfactors"
            show_questions
            
        Case "Psychological Factors"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_psychological", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            
            tablename = "q_obesity_psychological"
            show_questions
        
        Case "Evaluating Risk Factor"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_erf_personal", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            
            tablename = "q_obesity_erf_personal"
            show_questions
    '----END SUBTOPIC - CAUSES
           
    '----BEGIN SUBTOPIC - BMI
        Case "Body Mass Index"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_BMI", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_obesity_BMI"
            show_questions

    '----END SUBTOPIC - BMI
        
    '----BEGIN SUBTOPIC - Treatment
        Case "Diet"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_treatment_diet", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_obesity_treatment_diet"
            show_questions
            
        Case "Physical Activity"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_treatment_pa", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_obesity_treatment_diet"
            show_questions
            
        Case "Pharmacotherapy"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_obesity_treatment_pharma", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_obesity_treatment_pharma"
            show_questions
    '----END SUBTOPIC - Treatment
'----------------------END TOPIC: OBESITY

'----------------------BEGIN TOPIC: MALNUTRITION
    '----BEGIN SUBTOPIC - DIETARY MANAGEMENT
       Case "Fats"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_malnutrition_dm_fat", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_malnutrition_dm_fat"
            show_questions
            
        Case "Carbohydrates"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_malnutrition_dm_carbo", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_malnutrition_dm_carbo"
            show_questions
        
        Case "Energy"
            clear_prev_c
            Set rc = New ADODB.Recordset
                rc.Open "SELECT * FROM q_malnutrition_dm_energy", conJoey, adOpenForwardOnly, adLockReadOnly
            
            For x = 1 To rc.RecordCount
                Option1(x).Caption = rc("question")
                Option1(x).Visible = True
                rc.MoveNext
            Next x
            tablename = "q_malnutrition_dm_energy"
            show_questions
    '----END SUBTOPIC - DIETARY MANAGEMENT
'----------------------END MAIN TOPIC: MALNUTRITION

'----------------------BEGIN MAIN TOPIC: G&R
'----------------------END MAIN TOPIC: G&R
End Select

        
End Sub

Private Sub Option1_Click(Index As Integer)
AnswerIndex = Index
End Sub

Private Sub cmdexit_Click()
Unload Me
End Sub

