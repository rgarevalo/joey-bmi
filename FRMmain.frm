VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FRMmain 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "JOEY - BMI Calculator and Fitness Consultant System v1.0 - Siopao Corporation�"
   ClientHeight    =   8955
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8610
   Icon            =   "FRMmain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8955
   ScaleWidth      =   8610
   StartUpPosition =   2  'CenterScreen
   Begin ComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   435
      Left            =   0
      TabIndex        =   11
      Top             =   8520
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   767
      SimpleText      =   ""
      _Version        =   327682
      BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
         NumPanels       =   4
         BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Alignment       =   1
            AutoSize        =   1
            Bevel           =   2
            Object.Width           =   7435
            Text            =   "JOEY v1.0 2006 - Siopao Corporation�"
            TextSave        =   "JOEY v1.0 2006 - Siopao Corporation�"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Style           =   1
            Alignment       =   1
            Enabled         =   0   'False
            TextSave        =   "CAPS"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel3 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Style           =   2
            Alignment       =   1
            TextSave        =   "NUM"
            Object.Tag             =   ""
         EndProperty
         BeginProperty Panel4 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
            Style           =   5
            Alignment       =   1
            TextSave        =   "12:10 AM"
            Object.Tag             =   ""
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin TabDlg.SSTab MyTab 
      Height          =   7575
      Left            =   0
      TabIndex        =   10
      Top             =   960
      Width           =   8610
      _ExtentX        =   15187
      _ExtentY        =   13361
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "&BMI Calculation"
      TabPicture(0)   =   "FRMmain.frx":17D2A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "Frame1"
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "C&onsultation System"
      TabPicture(1)   =   "FRMmain.frx":17D46
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Frame20"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Past &User Record"
      TabPicture(2)   =   "FRMmain.frx":17D62
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame2"
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame20 
         BackColor       =   &H8000000E&
         Height          =   6975
         Left            =   240
         TabIndex        =   50
         Top             =   480
         Width           =   8175
         Begin VB.CommandButton cmdLaunch 
            Caption         =   "Ask JOEY!"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   1800
            TabIndex        =   62
            Top             =   4800
            Width           =   4335
         End
         Begin VB.CommandButton cmdexit 
            Caption         =   "&EXIT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Index           =   2
            Left            =   6960
            Style           =   1  'Graphical
            TabIndex        =   51
            Top             =   6240
            Width           =   975
         End
         Begin VB.Label Label27 
            BackStyle       =   0  'Transparent
            Caption         =   "JOEY� version 1.0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   6510
            TabIndex        =   59
            Top             =   455
            Width           =   1575
         End
         Begin VB.Label Label14 
            BackStyle       =   0  'Transparent
            Caption         =   "All rights reserved."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   58
            Top             =   6600
            Width           =   1455
         End
         Begin VB.Label Label10 
            BackStyle       =   0  'Transparent
            Caption         =   "Copyright 2006 Siopao Corporation."
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   240
            TabIndex        =   57
            Top             =   6360
            Width           =   2655
         End
         Begin VB.Label Label9 
            BackStyle       =   0  'Transparent
            Caption         =   "Consultation System"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   5040
            TabIndex        =   56
            Top             =   120
            Width           =   3015
         End
         Begin VB.Label Label6 
            BackStyle       =   0  'Transparent
            Caption         =   $"FRMmain.frx":17D7E
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   11.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3135
            Left            =   1800
            TabIndex        =   55
            Top             =   1560
            Width           =   4455
         End
         Begin VB.Image Image1 
            Height          =   480
            Left            =   4440
            Picture         =   "FRMmain.frx":17F74
            Top             =   120
            Width           =   480
         End
         Begin VB.Image Image3 
            Height          =   3960
            Left            =   1680
            Picture         =   "FRMmain.frx":1883E
            Stretch         =   -1  'True
            Top             =   1200
            Width           =   4620
         End
         Begin VB.Label Label13 
            BackColor       =   &H8000000D&
            Height          =   735
            Left            =   0
            TabIndex        =   54
            Top             =   0
            Width           =   8415
         End
         Begin VB.Label Label23 
            BackStyle       =   0  'Transparent
            Caption         =   "JOEY� version 1.0"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H8000000E&
            Height          =   255
            Left            =   6510
            TabIndex        =   53
            Top             =   450
            Width           =   1575
         End
         Begin VB.Label Label24 
            BackStyle       =   0  'Transparent
            Caption         =   "Consultation System"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   5040
            TabIndex        =   52
            Top             =   120
            Width           =   3015
         End
         Begin VB.Image Image4 
            Height          =   480
            Left            =   4440
            Picture         =   "FRMmain.frx":19A0F
            Top             =   120
            Width           =   480
         End
      End
      Begin VB.Frame Frame2 
         Height          =   6615
         Left            =   -74880
         TabIndex        =   41
         Top             =   360
         Width           =   8415
         Begin VB.CommandButton DELETEALL 
            Caption         =   "DELETE LAHAT"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   5880
            Style           =   1  'Graphical
            TabIndex        =   61
            Top             =   2040
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.CommandButton cmdreportview 
            Caption         =   "View In Report Format"
            Height          =   255
            Left            =   600
            TabIndex        =   60
            Top             =   5400
            Width           =   2175
         End
         Begin VB.Frame Frame19 
            Caption         =   "CONTROLS"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1095
            Left            =   3240
            TabIndex        =   44
            Top             =   5400
            Width           =   5055
            Begin VB.CommandButton cmdedit 
               Caption         =   "&EDIT"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   120
               Style           =   1  'Graphical
               TabIndex        =   49
               Top             =   240
               Width           =   975
            End
            Begin VB.CommandButton cmddelete1 
               Caption         =   "&DELETE"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   1080
               Style           =   1  'Graphical
               TabIndex        =   48
               Top             =   240
               Width           =   975
            End
            Begin VB.CommandButton cmdsave1 
               Caption         =   "&SAVE"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   2040
               Style           =   1  'Graphical
               TabIndex        =   47
               Top             =   240
               Width           =   975
            End
            Begin VB.CommandButton cmdprinter1 
               Caption         =   "&PRINT"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   3000
               Style           =   1  'Graphical
               TabIndex        =   46
               Top             =   240
               Width           =   975
            End
            Begin VB.CommandButton cmdexit 
               Caption         =   "E&XIT"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Index           =   1
               Left            =   3960
               TabIndex        =   45
               Top             =   240
               Width           =   975
            End
         End
         Begin MSDataGridLib.DataGrid DataGrid1 
            Bindings        =   "FRMmain.frx":1A2D9
            Height          =   4815
            Left            =   120
            TabIndex        =   42
            Top             =   360
            Width           =   8175
            _ExtentX        =   14420
            _ExtentY        =   8493
            _Version        =   393216
            AllowUpdate     =   0   'False
            HeadLines       =   1
            RowHeight       =   15
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   ""
               Caption         =   ""
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
               EndProperty
               BeginProperty Column01 
               EndProperty
            EndProperty
         End
         Begin MSAdodcLib.Adodc ado 
            Height          =   615
            Left            =   120
            Top             =   5760
            Width           =   3015
            _ExtentX        =   5318
            _ExtentY        =   1085
            ConnectMode     =   0
            CursorLocation  =   3
            IsolationLevel  =   -1
            ConnectionTimeout=   15
            CommandTimeout  =   30
            CursorType      =   3
            LockType        =   3
            CommandType     =   8
            CursorOptions   =   0
            CacheSize       =   50
            MaxRecords      =   0
            BOFAction       =   0
            EOFAction       =   0
            ConnectStringType=   1
            Appearance      =   1
            BackColor       =   -2147483643
            ForeColor       =   -2147483640
            Orientation     =   0
            Enabled         =   -1
            Connect         =   ""
            OLEDBString     =   ""
            OLEDBFile       =   ""
            DataSourceName  =   ""
            OtherAttributes =   ""
            UserName        =   ""
            Password        =   ""
            RecordSource    =   ""
            Caption         =   ""
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            _Version        =   393216
         End
      End
      Begin VB.Frame Frame1 
         Height          =   6615
         Left            =   -74880
         TabIndex        =   12
         Top             =   360
         Width           =   8415
         Begin VB.Frame Frame17 
            Height          =   2175
            Left            =   6960
            TabIndex        =   31
            Top             =   2880
            Width           =   1215
            Begin VB.CommandButton cmdexit 
               Caption         =   "E&XIT"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Index           =   0
               Left            =   120
               TabIndex        =   34
               Top             =   1440
               Width           =   975
            End
            Begin VB.CommandButton cmdcredits 
               Caption         =   "&CREDITS"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   120
               TabIndex        =   33
               Top             =   840
               Width           =   975
            End
            Begin VB.CommandButton cmdhelp 
               Caption         =   "&HELP"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   120
               Style           =   1  'Graphical
               TabIndex        =   32
               Top             =   240
               Width           =   975
            End
         End
         Begin VB.Frame Frame15 
            Height          =   2295
            Left            =   120
            TabIndex        =   24
            Top             =   2880
            Width           =   3615
            Begin VB.Frame Frame12 
               Height          =   975
               Left            =   1920
               TabIndex        =   29
               Top             =   1200
               Width           =   1575
               Begin VB.CommandButton cmdsave 
                  Caption         =   "&SAVE RECORD"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   6.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   615
                  Left            =   120
                  TabIndex        =   30
                  Top             =   240
                  Width           =   1335
               End
            End
            Begin VB.Frame Frame8 
               Height          =   975
               Left            =   120
               TabIndex        =   27
               Top             =   1200
               Width           =   1695
               Begin VB.CommandButton cmdconsul 
                  Caption         =   "PROCEED to &CONSULTATION"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   6.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   615
                  Left            =   120
                  TabIndex        =   28
                  Top             =   240
                  Width           =   1455
               End
            End
            Begin VB.Frame Frame14 
               Caption         =   "EVALUATION REMARKS"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   975
               Left            =   120
               TabIndex        =   25
               Top             =   240
               Width           =   3375
               Begin VB.Label lblEval 
                  Alignment       =   2  'Center
                  Appearance      =   0  'Flat
                  BackColor       =   &H80000005&
                  BorderStyle     =   1  'Fixed Single
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  ForeColor       =   &H80000008&
                  Height          =   615
                  Left            =   120
                  TabIndex        =   26
                  Top             =   240
                  Width           =   3135
               End
            End
         End
         Begin VB.Frame Frame13 
            Caption         =   "Weight (Choose unit)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1575
            Left            =   120
            TabIndex        =   23
            Top             =   1080
            Width           =   2295
            Begin VB.ComboBox cmbwunit 
               Height          =   315
               ItemData        =   "FRMmain.frx":1A2EB
               Left            =   720
               List            =   "FRMmain.frx":1A2F5
               Style           =   2  'Dropdown List
               TabIndex        =   36
               Top             =   1080
               Width           =   1455
            End
            Begin VB.TextBox txtw 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   120
               MaxLength       =   6
               TabIndex        =   4
               Top             =   360
               Width           =   2055
            End
            Begin VB.Label Label3 
               Caption         =   "Unit :"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   38
               Top             =   1080
               Width           =   1215
            End
         End
         Begin VB.Frame Frame10 
            Caption         =   "Height (Choose unit)"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1575
            Left            =   2640
            TabIndex        =   22
            Top             =   1080
            Width           =   2295
            Begin VB.ComboBox cmbhunit 
               Height          =   315
               ItemData        =   "FRMmain.frx":1A30B
               Left            =   720
               List            =   "FRMmain.frx":1A315
               Style           =   2  'Dropdown List
               TabIndex        =   37
               Top             =   1080
               Width           =   1455
            End
            Begin VB.TextBox txth 
               Alignment       =   2  'Center
               BackColor       =   &H00FFFFFF&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   18
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   555
               Left            =   120
               MaxLength       =   6
               TabIndex        =   5
               Top             =   360
               Width           =   2055
            End
            Begin VB.Label Label8 
               Caption         =   "Unit :"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   255
               Left            =   120
               TabIndex        =   39
               Top             =   1080
               Width           =   1215
            End
         End
         Begin VB.Frame Frame4 
            Caption         =   "Enter your complete name"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   120
            TabIndex        =   21
            Top             =   240
            Width           =   3855
            Begin VB.TextBox txtname 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   120
               MaxLength       =   25
               TabIndex        =   1
               Top             =   240
               Width           =   3615
            End
         End
         Begin VB.Frame Frame5 
            Caption         =   "Enter your age"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   4320
            TabIndex        =   20
            Top             =   240
            Width           =   1455
            Begin VB.TextBox txtage 
               Alignment       =   2  'Center
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   120
               MaxLength       =   2
               TabIndex        =   2
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame Frame9 
            Height          =   975
            Left            =   6720
            TabIndex        =   18
            Top             =   960
            Width           =   1455
            Begin VB.CommandButton cmdreset 
               Caption         =   "&RESET"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   120
               TabIndex        =   19
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame Frame6 
            Caption         =   "Specify Gender"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   615
            Left            =   6000
            TabIndex        =   17
            Top             =   240
            Width           =   2175
            Begin VB.ComboBox cmbGender 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               ItemData        =   "FRMmain.frx":1A328
               Left            =   240
               List            =   "FRMmain.frx":1A332
               Style           =   2  'Dropdown List
               TabIndex        =   3
               Top             =   240
               Width           =   1575
            End
         End
         Begin VB.Frame Frame7 
            Height          =   975
            Left            =   5040
            TabIndex        =   15
            Top             =   960
            Width           =   1455
            Begin VB.CommandButton cmdcalcBMI 
               Caption         =   "&CALCULATE BMI"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   6.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   615
               Left            =   120
               TabIndex        =   16
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame Frame11 
            Caption         =   "Body Mass Index Calculation"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FF0000&
            Height          =   735
            Left            =   5040
            TabIndex        =   14
            Top             =   2040
            Width           =   3135
            Begin VB.Label lblBMIresult 
               Alignment       =   2  'Center
               Appearance      =   0  'Flat
               BackColor       =   &H80000005&
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H80000008&
               Height          =   375
               Left            =   120
               TabIndex        =   40
               Top             =   240
               Width           =   2895
            End
         End
         Begin VB.Frame Frame16 
            Caption         =   "Current Date"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2655
            Left            =   3960
            TabIndex        =   13
            Top             =   2880
            Width           =   2895
            Begin MSComCtl2.MonthView MonthView1 
               Height          =   2370
               Left            =   120
               TabIndex        =   43
               Top             =   240
               Width           =   2700
               _ExtentX        =   4763
               _ExtentY        =   4180
               _Version        =   393216
               ForeColor       =   -2147483630
               BackColor       =   16777215
               Appearance      =   1
               StartOfWeek     =   20643841
               CurrentDate     =   38918
            End
         End
         Begin VB.Label Label7 
            BackStyle       =   0  'Transparent
            Caption         =   $"FRMmain.frx":1A344
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1335
            Left            =   720
            TabIndex        =   35
            Top             =   5280
            Width           =   3375
         End
         Begin VB.Image Image2 
            Height          =   480
            Left            =   120
            Picture         =   "FRMmain.frx":1A418
            Top             =   5400
            Width           =   480
         End
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   975
      Left            =   0
      Picture         =   "FRMmain.frx":1AA21
      ScaleHeight     =   915
      ScaleWidth      =   8595
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "JOEY"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   36
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFF00&
         Height          =   735
         Left            =   1470
         TabIndex        =   8
         Top             =   75
         Width           =   1935
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Fitness Consultant System"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   4200
         TabIndex        =   7
         Top             =   530
         Width           =   3135
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Body Mass Index Test Calculator and"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   3600
         TabIndex        =   6
         Top             =   210
         Width           =   4335
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "JOEY"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   36
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   735
         Left            =   1440
         TabIndex        =   9
         Top             =   120
         Width           =   1815
      End
   End
End
Attribute VB_Name = "FRMmain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim BMI As Single

Private Sub cmbwunit_Click()
If txtw.Text <> "" Then
    txtw.Text = ""
    txtw.SetFocus
End If
End Sub

Private Sub cmbhunit_Click()
If txth.Text <> "" Then
    txth.Text = ""
    txth.SetFocus
End If
End Sub

Private Sub cmdcalcBMI_Click()
If txtname.Text = "" Then
    MsgBox "Please enter your name!", vbExclamation, "Name missing"
    txtname.SetFocus
    Exit Sub
End If

If txtage.Text = "" Then
    MsgBox "Please enter your age!", vbExclamation, "Age missing"
    txtage.SetFocus
    Exit Sub
End If

If cmbGender.Text = "" Then
    MsgBox "You must select your gender!", vbExclamation, "Gender missing"
    cmbGender.SetFocus
    Exit Sub
End If

If txtw.Text = "" Then
    MsgBox "Please specify your weight!", vbExclamation, "Weight missing"
    txtw.SetFocus
    Exit Sub
End If

If txth.Text = "" Then
    MsgBox "Please specify your height!", vbExclamation, "Height missing"
    txth.SetFocus
    Exit Sub
End If

Dim userweight As Single
Dim userheight As Single

'WEIGHT CONVERSION
Select Case cmbwunit.ListIndex
    Case 0: userweight = CSng(txtw.Text) 'KG
    Case 1: userweight = CSng(txtw.Text) * 0.45 'LBS to KG
End Select

'HEIGHT CONVERSION
Select Case cmbhunit.ListIndex
    Case 0: userheight = CSng(txth.Text) 'METER
    Case 1
        Dim cm As Single
        Dim mtr As Single
        
        cm = CSng(txth.Text) * 2.54 'INCH to CM
        mtr = cm / 100 'CM to METER
        userheight = mtr
End Select

        
BMI = userweight / CSng(txth.Text) ^ 2 'BMI calculation
lblBMIresult.Caption = "Your BMI is " & BMI & " k/m" & Chr(178)
    

    'Convert KG to pound
    'PoundUnit = txtKg.Text * 2.2
    'If poundunit >= 125 and
If BMIresult <= 24 Then
    lblEval.Caption = "Your BMI is NORMAL."
    lblEval.ForeColor = &HFF8080
End If

If BMIresult >= 25 Then
    lblEval.Caption = "Your BMI is OVERWEIGHT."
    lblEval.ForeColor = &HFF8080
End If
    
If BMIresult >= 30 Then
    lblEval.Caption = "Your BMI is OBESE."
    lblEval.ForeColor = &HFF8080
End If
    
If BMIresult >= 40 Then
    lblEval.Caption = "Your BMI is EXTREMELY OBESE."
    lblEval.ForeColor = &HFF8080
End If

End Sub

Private Sub cmdconsul_Click()
MyTab.Tab = 1
End Sub

Private Sub cmdcredits_Click()
FRMabout.Show
End Sub

Private Sub cmddelete1_Click()
If ado.Recordset.RecordCount <> 0 Then
    If vbYes = MsgBox("Delete the current record?", vbQuestion + vbYesNo, "Confirm") Then
        ado.Recordset.Delete
        ado.Refresh
        DataGrid1.Refresh
        ado.Recordset.MoveNext
        
    End If
End If
End Sub

Private Sub cmdexit_Click(Index As Integer)
If vbYes = MsgBox("Do you want to exit the program?", vbQuestion + vbYesNo, "End Program") Then End
End Sub

Private Sub cmdhelp_Click()
FRMhelp.Show
End Sub

Private Sub cmdLaunch_Click()
OFRMindex.Show
End Sub

Private Sub cmdprint_Click()
On Error GoTo printer_error
retry:
    Printer.Print "Printer is working"
    Printer.EndDoc
Exit Sub
printer_error:
If Err.Number = 482 Then
    If vbRetry = MsgBox(Err.Description, vbExclamation + vbRetryCancel, "Error") Then
        GoTo retry
    End If
End If
End Sub

Private Sub cmdreportview_Click()
Dim inventory As ADODB.Recordset
connect_User
Set inventory = New ADODB.Recordset
    inventory.Open "SELECT * FROM patients_pro", conUser, adOpenDynamic, adLockOptimistic
Set RPTusereval.DataSource = inventory
RPTusereval.Show
End Sub

Private Sub cmdreset_Click()
If vbYes = MsgBox("Clear all information you have entered?", vbQuestion + vbYesNo, "Clear Form") Then
reset_fields
End If
End Sub

Private Sub reset_fields()
txtname.Text = ""
txtage.Text = ""
txtw.Text = ""
txth.Text = ""
    
lblBMIresult.Caption = ""
lblEval.Caption = ""
    
With cmbGender
    .Clear
    .AddItem "Male"
    .AddItem "Female"
End With
    
With cmbwunit
    .Clear
    .AddItem "Kilogram"
    .AddItem "Pounds"
    .ListIndex = 0
End With
    
With cmbhunit
    .Clear
    .AddItem "Meter"
    .AddItem "Inches"
    .ListIndex = 0
End With
        
txtname.SetFocus
End Sub

Private Sub cmdsave_Click()
If txtname.Text = "" Or txtage.Text = "" Or cmbGender.Text = "" Or _
    txtw.Text = "" Or txth.Text = "" Then
    MsgBox "You must fill up all information before you can save your diagnostic results!", vbExclamation, "Some information missing"
    
    If txtname.Text = "" Then txtname.SetFocus: Exit Sub
    If txtage.Text = "" Then txtage.SetFocus: Exit Sub
    If cmbGender.Text = "" Then cmbGender.SetFocus: Exit Sub
    If txtw.Text = "" Then txtw.SetFocus: Exit Sub
    If txth.Text = "" Then txth.SetFocus: Exit Sub
    Exit Sub
End If

If txtw.Text <> "" And txth.Text <> "" Then
    cmdcalcBMI.Value = True
End If

Dim rc As New ADODB.Recordset
connect_User

rc.Open "SELECT * FROM patients_pro", conUser, adOpenKeyset, adLockOptimistic
rc.AddNew
rc("Name") = txtname.Text
rc("Age") = txtage.Text
rc("Gender") = cmbGender.Text
rc("Weight") = txtw.Text & " " & cmbwunit.Text
rc("Height") = txth.Text & " " & cmbhunit.Text
rc("BMI") = BMI & " kg/m" & Chr(178)
rc("Date") = Date
rc("Evaluation") = lblEval.Caption
rc.Update
rc.Close
conUser.Close
reset_fields
End Sub

Private Sub cmdprinter1_Click()
If vbYes = MsgBox("Print these records?", vbQuestion + vbYesNo, "Confirm") Then
On Error GoTo printer_error
Dim x As Integer
retry:
    Printer.FontSize = 10
    Printer.Print "JOEY BMI Body Mass Index Test Calculator "
    Printer.Print "and Fitness Consultant System"
    
    Printer.Print "Name"
    ado.Recordset.MoveFirst
            Printer.Print "-----------------"
            Printer.Print "PERSONAL INFORMATION"
            Printer.Print "Name:"; ado.Recordset("Name")
            Printer.Print "Age:"; ado.Recordset("Age")
            Printer.Print "Gender:"; ado.Recordset("Gender")
            Printer.Print
            Printer.Print "STATUS"
            Printer.Print "Weight:"; ado.Recordset("Weight")
            Printer.Print "Height:"; ado.Recordset("Height")
            Printer.Print "BMI:"; ado.Recordset("BMI")
            Printer.Print "Date:"; ado.Recordset("Date")
            Printer.Print "Evaluation:"; ado.Recordset("Evaluation")
            Printer.Print "-----------------"
    Printer.EndDoc
End If
Exit Sub
printer_error:
If Err.Number = 482 Then
    If vbRetry = MsgBox(Err.Description, vbExclamation + vbRetryCancel, "Error") Then
        GoTo retry
    End If
End If
End Sub

Private Sub cmdedit_Click()
DataGrid1.AllowUpdate = True
End Sub

Private Sub cmdsave1_Click()
If ado.Recordset.RecordCount <> 0 Then
    If DataGrid1.AllowUpdate = True Then
        MsgBox "Modified record(s) successfully updated!", vbInformation, "Database updated"
        ado.Recordset.Update
    End If
End If
End Sub

'DI KASAMA SA MAIN FUNCTION NG PROGRAM TO! PANGBURA LANG NG TEST RECORDS
Private Sub DELETEALL_Click()
Dim x As Integer


If ado.Recordset.RecordCount <> 0 Then
    ado.Recordset.MoveFirst
    For x = 1 To ado.Recordset.RecordCount
        ado.Recordset.Delete
        ado.Recordset.MoveNext
    Next x
 
End If
ado.Refresh
End Sub

Private Sub Form_Activate()
txtname.SetFocus
End Sub

Private Sub Form_Load()
cmbwunit.ListIndex = 0
cmbhunit.ListIndex = 0

'Runtime connection string
ado.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\JoeyUserdb.mdb;Persist Security Info=False"
ado.CommandType = adCmdText
ado.RecordSource = "SELECT * FROM patients_pro"
Set DataGrid1.DataSource = ado
End Sub

Private Sub MyTab_Click(PreviousTab As Integer)
If MyTab.Tab = 2 Then ado.Refresh
End Sub

Private Sub txtage_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case vbKey0 To vbKey9, vbKeyBack, 44, 46
    Case Else: KeyAscii = 0
End Select
End Sub

Private Sub txtname_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case 65 To 90, 97 To 122, vbKeyBack, 32, 46
    Case Else: KeyAscii = 0
End Select
End Sub

Private Sub txtw_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case vbKey0 To vbKey9, vbKeyBack, 46
    Case Else: KeyAscii = 0
End Select
End Sub

Private Sub txth_KeyPress(KeyAscii As Integer)
Select Case KeyAscii
    Case vbKey0 To vbKey9, vbKeyBack, 46
    Case Else: KeyAscii = 0
End Select
End Sub

