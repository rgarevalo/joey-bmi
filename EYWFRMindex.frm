VERSION 5.00
Begin VB.Form EYWFRMindex 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Evaluating your weight"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8040
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   8040
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdexit 
      Caption         =   "E&XIT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   1
      Left            =   6720
      TabIndex        =   1
      Top             =   3960
      Width           =   975
   End
   Begin VB.Frame Frame1 
      Height          =   3735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7575
   End
End
Attribute VB_Name = "EYWFRMindex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdexit_Click(Index As Integer)
Unload Me
End Sub

Private Sub Form_Load()
FRMmain.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
FRMmain.Enabled = True
End Sub

