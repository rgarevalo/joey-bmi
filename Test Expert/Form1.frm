VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   7170
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9090
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7170
   ScaleWidth      =   9090
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdseek 
      Caption         =   "Seek"
      Height          =   375
      Left            =   7718
      TabIndex        =   6
      Top             =   240
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   120
      MultiLine       =   -1  'True
      TabIndex        =   4
      Top             =   4440
      Width           =   8775
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3000
      Left            =   158
      TabIndex        =   3
      Top             =   840
      Width           =   8775
   End
   Begin VB.ComboBox Combo1 
      Height          =   315
      ItemData        =   "Form1.frx":0000
      Left            =   5078
      List            =   "Form1.frx":000D
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   240
      Width           =   2535
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   158
      TabIndex        =   0
      Top             =   240
      Width           =   3615
   End
   Begin VB.Label Label2 
      Caption         =   "Answer:"
      Height          =   255
      Left            =   158
      TabIndex        =   5
      Top             =   4080
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Search In:"
      Height          =   255
      Left            =   3998
      TabIndex        =   2
      Top             =   240
      Width           =   1215
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim conkb As New ADODB.Connection

Dim lastTable As String

Private Sub cmdseek_Click()
Dim find As ADODB.Recordset
Dim x As Integer

Set find = New ADODB.Recordset

If Text1.Text = "" Then
    MsgBox "Your query is required!", vbExclamation, "Query Missing"
    Text1.SetFocus
    Exit Sub
End If


If Combo1.ListIndex = 1 Then
    find.Open "SELECT * FROM q_obesity WHERE question LIKE '%" & Trim(Text1.Text) & "%'", conkb, adOpenKeyset, adLockOptimistic
    
    If find.RecordCount <> 0 Then
        List1.Clear
        For x = 1 To find.RecordCount
            List1.AddItem find("question")
            find.MoveNext
        Next x
        lastTable = "q_obesity"
    Else
        MsgBox "No record found!", vbExclamation, "Error"
        List1.Clear
    End If

ElseIf Combo1.ListIndex = 2 Then
    find.Open "SELECT * FROM q_obesity WHERE question LIKE '%" & Trim(Text1.Text) & "%'", conkb, adOpenKeyset, adLockOptimistic
    
    If find.RecordCount <> 0 Then
        List1.Clear
        For x = 1 To find.RecordCount
            List1.AddItem find("question")
            find.MoveNext
        Next x
        lastTable = "q_obesity"
    Else
        MsgBox "No record found!", vbExclamation, "Error"
        List1.Clear
    End If
End If
End Sub

Private Sub Form_Load()
conkb.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & App.Path & "\Joeykb.mdb;Persist Security Info=False"
End Sub

Private Sub List1_Click()
Dim answer As ADODB.Recordset

Set answer = New ADODB.Recordset
    answer.Open "SELECT * FROM " & lastTable & " WHERE question='" & List1.Text & "'", conkb, adOpenKeyset, adLockOptimistic
    Text2.Text = answer("answer")
End Sub
