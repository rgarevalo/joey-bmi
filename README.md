**JOEY™ - Body Mass Index Test Calculator and Fitness Consultant System**

This will explain various part of the Joey Interface.

**BMI Calculation Tab**
- You can find out your BMI rating here by entering information on all given fields
![BMI Calculation.PNG](https://bitbucket.org/repo/EA6X99/images/1008980269-BMI%20Calculation.PNG)

Remember:
WEIGHT must be in Kilogram or Pound unit.
HEIGHT must be in Meter or Inches unit


**Consultation System Tab**
- Find an answer fast! After you have learned about your BMI calculation, you  might 
as well come up with a question or two regarding your current status. JOEY™  includes 
a diagnostic portion that help isolate and resolve specific  questions regarding obesity, 
malnourishment and the normal health status. It also renders possible recommendations 
for improving current health status if necessary. Select from the categories in this 
section that will help you understand more about your physical fitness.
![Consultation System.PNG](https://bitbucket.org/repo/EA6X99/images/3242321230-Consultation%20System.PNG)

**Past User Record Tab**
- Here you can Edit, Delete, View or Print past user records that takes up BMI evaluation.
![History.PNG](https://bitbucket.org/repo/EA6X99/images/3286077503-History.PNG)

**About**

![About.PNG](https://bitbucket.org/repo/EA6X99/images/1618226357-About.PNG)