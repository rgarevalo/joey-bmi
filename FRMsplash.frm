VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form FRMsplash 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   ClientHeight    =   5415
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   ScaleHeight     =   5415
   ScaleWidth      =   6975
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Delayer 
      Interval        =   300
      Left            =   3120
      Top             =   480
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      Height          =   5175
      Left            =   188
      TabIndex        =   0
      Top             =   120
      Width           =   6615
      Begin VB.PictureBox Picture1 
         Height          =   975
         Left            =   -1320
         Picture         =   "FRMsplash.frx":0000
         ScaleHeight     =   915
         ScaleWidth      =   8595
         TabIndex        =   2
         Top             =   0
         Width           =   8655
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Body Mass Index Test Calculator and"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   375
            Left            =   3360
            TabIndex        =   5
            Top             =   210
            Width           =   4335
         End
         Begin VB.Label Label2 
            BackStyle       =   0  'Transparent
            Caption         =   "Fitness Consultant System"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   255
            Left            =   4200
            TabIndex        =   4
            Top             =   530
            Width           =   3135
         End
         Begin VB.Label Label5 
            BackStyle       =   0  'Transparent
            Caption         =   "JOEY"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   36
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFF00&
            Height          =   735
            Left            =   1470
            TabIndex        =   3
            Top             =   75
            Width           =   1935
         End
         Begin VB.Label Label4 
            BackStyle       =   0  'Transparent
            Caption         =   "JOEY"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   36
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   735
            Left            =   1440
            TabIndex        =   6
            Top             =   120
            Width           =   1815
         End
      End
      Begin ComctlLib.ProgressBar Loader 
         Height          =   375
         Left            =   180
         TabIndex        =   7
         Top             =   4680
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   661
         _Version        =   327682
         Appearance      =   1
         Max             =   40
      End
      Begin VB.Image Image1 
         Height          =   2970
         Left            =   480
         Picture         =   "FRMsplash.frx":3160
         Top             =   1200
         Width           =   5700
      End
      Begin VB.Label task 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   180
         TabIndex        =   1
         Top             =   4320
         Width           =   6135
      End
   End
End
Attribute VB_Name = "FRMsplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ctr As Integer

Private Sub Delayer_Timer()
Loader.Value = Loader.Value + 1

If Loader.Value <= 10 Then
    task.Caption = "Loading BMI calculation system."
ElseIf Loader.Value <= 20 Then
    task.Caption = "Loading Consultation System."
ElseIf Loader.Value <= 30 Then
    task.Caption = "Loading User records portion."
ElseIf Loader.Value >= 40 Then
    task.Caption = "Loading Complete. Press any key to continue."
    Delayer.Enabled = False
    Me.KeyPreview = True
End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
Unload Me
FRMmain.Show
End Sub

