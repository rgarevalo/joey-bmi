VERSION 5.00
Begin VB.Form FRMabout 
   BackColor       =   &H00C0FFFF&
   BorderStyle     =   0  'None
   Caption         =   "Credits"
   ClientHeight    =   6000
   ClientLeft      =   0
   ClientTop       =   -105
   ClientWidth     =   6420
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6000
   ScaleWidth      =   6420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   4440
      Top             =   4320
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "CLOSE"
      Height          =   495
      Left            =   2603
      TabIndex        =   14
      Top             =   5400
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0FFFF&
      Height          =   4695
      Left            =   90
      TabIndex        =   0
      Top             =   120
      Width           =   6240
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "John Paul Mamaril - Researcher, Producer"
         Height          =   255
         Left            =   480
         TabIndex        =   13
         Top             =   4200
         Width           =   5055
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Bryan Jason Callos - Researcher"
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   3960
         Width           =   5055
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "and..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   3720
         Width           =   5055
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Robertson Paredes - Researcher"
         Height          =   255
         Left            =   480
         TabIndex        =   10
         Top             =   3000
         Width           =   5055
      End
      Begin VB.Label Label9 
         BackStyle       =   0  'Transparent
         Caption         =   "Dennes Parojenog - Researcher, Knowledge Engineer"
         Height          =   255
         Left            =   480
         TabIndex        =   9
         Top             =   2760
         Width           =   5055
      End
      Begin VB.Label Label8 
         BackStyle       =   0  'Transparent
         Caption         =   "Randolf G. Arevalo - Lead Programmer, Knowledge Engineer"
         Height          =   255
         Left            =   480
         TabIndex        =   8
         Top             =   2520
         Width           =   5055
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Angelo Gabriel Oquialda - Researcher, Knowledge Engineer"
         Height          =   255
         Left            =   480
         TabIndex        =   7
         Top             =   1320
         Width           =   5055
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "Diana Marie Jorillo - Researcher, Programmer"
         Height          =   255
         Left            =   480
         TabIndex        =   6
         Top             =   1080
         Width           =   5055
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "James Paolo Angeles - Team Leader"
         Height          =   255
         Left            =   480
         TabIndex        =   5
         Top             =   840
         Width           =   5055
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Parokya ni Yayo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   2160
         Width           =   5055
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "In Cooperation with"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   1920
         Width           =   5055
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Siopao Corporation"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   5055
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Developed By"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5055
      End
   End
   Begin VB.Label lblscroll 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "2006 - AMA Computer College Bi�an Campus"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   6360
      TabIndex        =   15
      Top             =   4920
      Width           =   4845
   End
End
Attribute VB_Name = "FRMabout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdClose_Click()
Unload Me
End Sub

Private Sub Form_Load()
FRMmain.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
FRMmain.Enabled = True
End Sub

Private Sub Timer1_Timer()
If lblscroll.Left <= 6360 Then
    lblscroll.Left = lblscroll.Left - 100
    
    If lblscroll.Left <= -6360 Then
        lblscroll.Left = 6360
    End If
End If

End Sub
