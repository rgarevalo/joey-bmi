VERSION 5.00
Begin VB.Form SFFRMindex 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Staying Fit Index"
   ClientHeight    =   7950
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8625
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7950
   ScaleWidth      =   8625
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdexit 
      Caption         =   "E&XIT"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Index           =   1
      Left            =   7560
      TabIndex        =   0
      Top             =   7200
      Width           =   975
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0FF&
      Height          =   6975
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8415
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   9
         Left            =   360
         TabIndex        =   17
         Top             =   5760
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   8
         Left            =   360
         TabIndex        =   16
         Top             =   5160
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   6
         Left            =   360
         TabIndex        =   15
         Top             =   3960
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   7
         Left            =   360
         TabIndex        =   14
         Top             =   4560
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   5
         Left            =   360
         TabIndex        =   10
         Top             =   3360
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   2
         Left            =   360
         TabIndex        =   6
         Top             =   1560
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   360
         TabIndex        =   5
         Top             =   960
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   3
         Left            =   360
         TabIndex        =   4
         Top             =   2160
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.OptionButton myopt 
         BackColor       =   &H00FFFFFF&
         Height          =   495
         Index           =   4
         Left            =   360
         TabIndex        =   3
         Top             =   2760
         Visible         =   0   'False
         Width           =   7815
      End
      Begin VB.CommandButton cmdnext 
         Caption         =   "&NEXT"
         Height          =   495
         Left            =   360
         TabIndex        =   2
         Top             =   6360
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "What is your question?"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   7
         Top             =   600
         Width           =   3495
      End
   End
   Begin VB.Frame frmAns 
      BackColor       =   &H00C0FFFF&
      Height          =   6975
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Visible         =   0   'False
      Width           =   8415
      Begin VB.Frame Frame2 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Are you satisfied with the result?"
         ForeColor       =   &H80000008&
         Height          =   855
         Left            =   120
         TabIndex        =   11
         Top             =   6000
         Width           =   2775
         Begin VB.CommandButton Command1 
            Caption         =   "Yes"
            Height          =   495
            Left            =   120
            TabIndex        =   13
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton cmdback 
            Caption         =   "No"
            Height          =   495
            Left            =   1440
            TabIndex        =   12
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Label lblans 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   8175
      End
   End
End
Attribute VB_Name = "SFFRMindex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim myquery As ADODB.Recordset
Dim x As Integer

Private Sub cmdback_Click()
frmAns.Visible = False
End Sub

Private Sub cmdnext_Click()
Dim showans As ADODB.Recordset

If myopt(1).Value = True Then
        frmAns.Visible = True
        Set showans = New ADODB.Recordset
        showans.Open "SELECT * FROM q_stayingfit WHERE question='" & myopt(1).Caption & "'", conJoey, adOpenKeyset, adLockOptimistic
        lblans.Caption = showans("answer")
ElseIf myopt(2).Value = True Then
        frmAns.Visible = True
        Set showans = New ADODB.Recordset
        showans.Open "SELECT * FROM q_stayingfit WHERE question='" & myopt(2).Caption & "'", conJoey, adOpenKeyset, adLockOptimistic
        lblans.Caption = showans("answer")
ElseIf myopt(3).Value = True Then
        frmAns.Visible = True
        Set showans = New ADODB.Recordset
        showans.Open "SELECT * FROM q_stayingfit WHERE question='" & myopt(3).Caption & "'", conJoey, adOpenKeyset, adLockOptimistic
        lblans.Caption = showans("answer")
End If
End Sub

Private Sub Form_Load()
FRMmain.Enabled = False


connect_Joey
Set myquery = New ADODB.Recordset
    myquery.Open "SELECT * FROM q_stayingfit", conJoey, adOpenKeyset, adLockOptimistic
    
For x = 1 To myquery.RecordCount
    myopt(x).Caption = myquery("question")
    myopt(x).Visible = True
    myquery.MoveNext
Next x
End Sub

Private Sub Form_Unload(Cancel As Integer)
FRMmain.Enabled = True
End Sub

